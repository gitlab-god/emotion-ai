<h1>Emotion AI<h1>

![Banner_Emotion_AI](Banner_Emotion_AI.jpg?raw=true "Banner Emotion AI")

**Autores**: Jose Luis Soto Soto, Jhon Anderson Ramírez Contreras, Daniel Alejandro Castillo Rodriguez

**Objetivo**: Identificar las emociones apartir de la imagen de un rostro humano.

[**Dataset**](data/icml_face_data.csv?raw=true): Más de 24000 registros de ubicaciones de pixeles de imagenes de rostros de personas.

**Librerias**: pandas, numpy, PIL, seaborn, pickle, cv2, tensorflow(keras, resnet50, densenet121), matploplib

**Modelos**: Deep Learning, Convolutional Neural Networks, Emotion Detecting

[**Notebook**](https://gitlab.com/gitlab-god/emotion-ai/-/blob/master/Emotion_AI_Final.ipynb)

[**Video**](https://www.youtube.com/watch?v=F5JlnytKhmc)

[**Diapositvas**](https://gitlab.com/gitlab-god/emotion-ai/-/blob/master/Emotion_AI.pdf)

[**Repositorio**](https://gitlab.com/gitlab-god/emotion-ai)
